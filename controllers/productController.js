const Product = require("../models/productsModel")
const auth = require("../auth");
const ObjectId = require('mongoose').Types.ObjectId;

const addProduct = async (req, res) => {
    try {
        // const token = req.headers.authorization;
        // const userData = auth.decode(token);

        const { name, description, price, stocks, path } = req.body

        let emptyFields = []

        if (!name) {
            emptyFields.push('name')
        }
        if (!description) {
            emptyFields.push('description')
        }
        if (!price) {
            emptyFields.push('price')
        }
        if (!stocks) {
            emptyFields.push('stocks')
        }
        if (!path) {
            emptyFields.push('path')
        }

        if (emptyFields.length > 0) {
            return res.status(400).json({ error: 'Please fill in all the fields', emptyFields })
        }

        // if(userData.isAdmin){
        const checkDuplicateProduct = await Product.findOne({ name: req.body.name, description: description })

        if (checkDuplicateProduct)
            return res.status(404).json({ error: "The product you entered is already exist!" })

        const product = await new Product({
            name,
            description,
            price,
            stocks,
            path
        })
        product.isActive = product.stocks == 0 ? false : true;
        const newProduct = await Product.create(product)
        return res.status(200).json(newProduct)
        // }
        // res.status(400).json({error:"Only admin can add a products!"})
    } catch (err) {
        res.status(400).json({ error: err.message })
    }
}

const getProduct = async (req, res) => {
    try {
        const product = await Product.findOne({ _id: req.params.productId })
        if (product) {
            return res.status(200).json(product)
        }
        res.status(400).json({ error: "The product you are trying to search is not existing!" })
    } catch (err) {
        res.status(400).json({ error: err.message })
    }
}

const getProductByName = async (req, res) => {
    try {
        const product = await Product.findOne({ name: req.body.name })
        if (product) {
            if (product.isActive) {
                return res.status(200).json(product)
            }
            else {
                res.json({ error: "Only admin allowed to do this!" })
            }
        }
        res.status(400).json({ error: "The product you are trying to search is not existing!" })
    } catch (err) {
        res.status(400).json({ error: err.message })
    }
}


const allProducts = async (req, res) => {
    try {
        //     const token = req.headers.authorization;
        //     const userData = auth.decode(token);

        //     if (userData.isAdmin) {
        const allProducts = await Product.find({}).sort({ createdAt: -1 });
        res.status(200).json(allProducts);

        // else {
        //     res.status(400).json({ error: "Only admin is allowed to do this action." })
        // }
    } catch (err) {
        res.status(405).json({ error: "err.message" })
    }
}


const allActiveProducts = async (req, res) => {
    try {
        // const token = req.headers.authorization;
        // const userData = auth.decode(token);

        // if(userData.isAdmin){
        const products = await Product.find({ isActive: true }).sort({ createdAt: -1 })
        res.status(200).json(products)
        // }
        // res.status(400).json({error: "Only admin can retrive all active products!"})
    } catch (err) {
        res.status(405).json({ error: "err.message" })
    }
}

const updateProducts = async (req, res) => {
    try {
        const productId = req.params.productId;
        if (!ObjectId.isValid(productId)) {
            return res.status(405).json({ error: `Product Id ${productId} is not valid objectId!` });
        }

        const token = req.headers.authorization;
        const userData = auth.decode(token)
        if (userData.isAdmin) {
            const productToUpdate = req.params.productId

            const updatedProduct = await Product.findOneAndUpdate({ _id: productToUpdate }, {
                ...req.body
            }, { new: true })

            updatedProduct.isActive = updatedProduct.stocks == 0 ? false : true;
            await updatedProduct.save();
            if (updatedProduct) {
                return res.status(200).json(updatedProduct)
            }
            res.status(400).json({ error: "Sorry, The product you are trying to update is not existing!" })
        }
        res.status(400).json({ error: "Sorry, But only admin is allowed to update a products!" })
    } catch (err) {
        res.status(400).json({ error: err.message })
    }
}

const archiveProduct = async (req, res) => {
    try {
        // const token = req.headers.authorization;
        // const userData = auth.decode(token)

        const { productId } = req.params
        // if(userData.isAdmin){
        const productData = await Product.findOne({ _id: productId })

        const productToArchive = await Product.findOneAndUpdate({ _id: productId }, { isActive: !productData.isActive }, { new: true })
        const products = await Product.find({})

        if (!productToArchive)
            return res.status(400).json({ error: "No such products!" })

        if (!products)
            return res.status(400).json({ error: "There is currently no available products!" })

        res.status(200).json(products)

        // }
    } catch (err) {
        res.status(400).json({ error: err.message })
    }
}

const deleteProduct = async (req, res) => {
    const token = req.headers.authorization
    const userData = auth.decode(token)

    if (!userData.isAdmin) return res.status(400).json({ error: "Only Admin Allowed to do this!" })

    const { _id } = req.body
    const productToDelete = await Product.findByIdAndDelete({ _id })

    if (!productToDelete)
        return res.status(404).json({ error: "No such product" })

    const updatedProducts = await Product.find({})
    res.status(200).json(updatedProducts)
}



module.exports = {
    addProduct,
    allActiveProducts,
    allProducts,
    updateProducts,
    archiveProduct,
    getProduct,
    getProductByName,
    deleteProduct
}