const User = require("../models/usersModel")
const bcrypt = require("bcrypt")
const auth = require("../auth")
const validator = require('validator')
const ObjectId = require('mongoose').Types.ObjectId;


const registerUser = async (req, res) => {

    try{
        const { email, password, confirmpassword } = req.body

        if (!email || !password || !confirmpassword) {
            return res.status(400).json({error: 'All fields must be filled!'})
        }
        if (!validator.isEmail(email)) {
            return res.status(400).json({error: 'Email is not valid!'})
        }
        if (!validator.isStrongPassword(password)) {
            return res.status(400).json({error: 'Password not strong enough!'})
        }

        const checkDuplicateEmail = await User.findOne({email : req.body.email});
    
        if(!checkDuplicateEmail){
            const user = new User({
                email: req.body.email,
                password: bcrypt.hashSync(req.body.password, 10)
            })
            if(req.body.confirmpassword !== req.body.password){
                return res.status(400).json({error: "Password does not match!"})
            }
            try{
                const newUser = await User.create(user);
                const token = auth.createToken(newUser)
                res.status(200).json({email, token});
            }catch (err) {
                res.status(400).json({error: err.message});
            }
        }
        else{
            res.status(400).json({error: "The email is already taken! Please choose another one."});
        }
    } catch(err) {
        res.status(400).json({error: err.message});
    }
}

const loginUser = async (req, res) => {
    try{
        const { email, password } = req.body
        const loginCreds = await User.findOne({email});
        
        if (!email || !password)
            return res.status(400).json({error: 'All fields must be filled!'})
        
        if(!loginCreds)
            return res.status(404).json({error:`The email you entered is not yet registered. Please register first!`});
        
        const isPasswordCorrect = await bcrypt.compareSync(password, loginCreds.password);

        if(!isPasswordCorrect)
            return res.status(404).json({error: `Password is incorrect!`});
        
        const token = auth.createToken(loginCreds)
        const isAdmin = loginCreds.isAdmin
        const _id = loginCreds._id
        console.log({email, token, isAdmin})
        res.status(200).json({email, token, isAdmin, _id});
        
    }catch(err){
        res.status(400).json({error : err.message})
    }
}


const getAllUsers = async (req, res) => {
    try{
        const token = req.headers.authorization
        const userData = auth.decode(token)
        const allUsers = await User.find({});
        // if(userData.isAdmin)
            res.status(200).json(allUsers);
        // else{
        //     res.status(400).json({error: "Only admin can perform this action!"})
        // }
    }catch(err){
        res.status(400).json({error: err.message});
    }
}



const updateRole = async (req, res) => {
    try{
        const productId = req.params.id;
        if(!ObjectId.isValid(productId)){
            return res.status(400).json({error: `Product Id ${productId} is not valid objectId!`});
        }
        const token = req.headers.authorization
        const userData = auth.decode(token)
        const userTobeUpdated = req.params.id

        if(userData.isAdmin){
            const result = await User.findById(userTobeUpdated)
            let updateUser = {
                isAdmin: !result.isAdmin
            }
            const updatedUser = await User.findByIdAndUpdate(userTobeUpdated, updateUser, {new: true})
            if(updatedUser){
            res.status(200).json(updatedUser)
            }
            else{
                res.status(400).json({error: "User not found!"})
            }
        }
        else{
            res.status(400).json({error: "You dont have acces on this page"})
        }
    }catch(err){
        res.status(400).json({error : err.message})
    }
}

const decodeUserToken = async (req, res) => {
    try{
        const token = req.headers.authorization
        const userData = auth.decode(token)

        if(!token)
            return res.status.json(404).json({error: "invalid token"})
        
        res.status(200).json(userData)
    }catch(err){
        res.status(400).json({error : err.message})
    }
}

module.exports = {
    registerUser,
    updateRole,
    loginUser,
    getAllUsers,
    decodeUserToken
}