const express = require("express")
const auth = require("../auth")

const router = express.Router();

const {
    registerUser, 
    loginUser,
    updateRole,
    getAllUsers,
    decodeUserToken

} = require("../controllers/usersController")

router.post("/register", registerUser)
router.get("/allUsers", getAllUsers)
router.post("/login", loginUser)
router.get("/decode", auth.verify, decodeUserToken)
router.patch("/updateRole/:id", auth.verify, updateRole)


module.exports = router;