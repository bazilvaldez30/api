const express = require("express");
const auth = require("../auth");

const router = express.Router();

const {
    addProductToCart,
    editQuantity,
    removeProductFromCart,
    updateByInputQuantity,
    checkOutOrders,
    checkYourCart,
    checkAllCarts,
    getAllUserTransaction,
    decreseQuantity,
    getAllTransaction,
    buyNow
} = require("../controllers/cartController")

router.post("/addToCart", auth.verify, addProductToCart)
router.put("/editProductQuantity", auth.verify, editQuantity)
router.put("/decreaseProductQuantity", auth.verify, decreseQuantity)
router.put("/updateByInputQuantity", auth.verify, updateByInputQuantity)
router.post("/checkOutOrder", auth.verify, checkOutOrders)
router.post("/buyNow", auth.verify, buyNow)
router.get("/checkYourCart", auth.verify, checkYourCart)
router.get("/checkAllCarts", auth.verify, checkAllCarts)
router.get("/checkTransactions", auth.verify, getAllUserTransaction)
router.get("/checkAllTransactions", auth.verify, getAllTransaction)
router.delete("/removeProduct/:productId", auth.verify, removeProductFromCart)

module.exports = router;
