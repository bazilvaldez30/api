const express = require("express");
const auth = require("../auth")

const router = express.Router();

const {
    addProduct,
    allActiveProducts,
    allProducts,
    updateProducts,
    archiveProduct,
    getProduct,
    getProductByName,
    deleteProduct
} = require("../controllers/productController")


router.get("/allActiveProducts", allActiveProducts)
router.get("/allProducts", allProducts)
router.get("/specificProduct", getProductByName)
router.post("/addProducts", addProduct)
router.delete("/deleteProduct", deleteProduct)
router.get("/specificProduct/:productId", getProduct)
router.patch("/updateProducts/:productId", auth.verify, updateProducts)
router.patch("/archiveProducts/:productId", auth.verify, archiveProduct)

module.exports = router;