const jwt = require("jsonwebtoken");
const User = require('./models/usersModel')

const secret = "EcommerceAPI"


const createToken = (loginCreds) => {
    const data = {
        _id: loginCreds._id,
        isAdmin: loginCreds.isAdmin,
    }
    return jwt.sign(data, secret, {expiresIn: '7d'})
}


const verify = async (req, res, next) => {

    const { authorization } = req.headers;
    
    
    if(!authorization) {
        return res.status(404).json({error:"Authentication failed! No Token provided"})
    }

    const token = authorization.split(" ")[1];

    try {
        const {_id} = jwt.verify(token, secret)
        req.user = await User.findOne({ _id }).select('_id')
        next()

    } catch (error) {
        console.log(error)
        res.status(405).json({error: "Request is not authorized"})
    }



    // return jwt.verify(token, secret, (error) => {
    //     if(error) {
    //         return res.status(404).json({error:"Invalid Token"});
    //     }
    //     next();  
    // })
    
}

const decode = (token) => {
    if(token === undefined){
        return null
    }
    else{
        token = token.slice(7, token.length)
        return jwt.verify(token, secret, (error, data) => {
            if(error){
                return null;
            }else{
                return jwt.decode(token, {complete: true}).payload
            }
        })
    }
}


module.exports = {
    createToken,
    verify,
    decode
}