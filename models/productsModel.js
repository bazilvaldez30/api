const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const productSchema = new Schema({
    name: {
        type: String,
        required: [true, "Product name is required"]
    },
    path:{
        type: String,
        default: ""
    },
    image: {
        type: Array,
        default: []
    },
    description: {
        type: String,
        required: [true, "Product description is required"]
    },
    price: {
        type: Number,
        required: [true, "Product price is required"]
    },
    stocks:{
        type: Number,
        required: [true, "Product stocks is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    }
}, {timestamps: true});

module.exports = mongoose.model("Product", productSchema);